#!/usr/bin/env bash

source set-namespace.sh

source set-kubernetes-vars.sh

kubectl apply -f ingress/ingress-controller.yaml --namespace="${NAMESPACE}"
