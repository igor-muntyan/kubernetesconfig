#!/usr/bin/env bash

#
# Prerequisites:
# 1. Install AWS CLI
# 2. Configure AWS profile
# 3. generate the $HOME/.ssh/kube_aws_rsa key
# 4. download the kubernetes (assuming into k8s/1.4.4)
#
export KUBERNETES_HOME="${HOME}/k8s/1.4.4"
#
export PATH=${KUBERNETES_HOME}/kubernetes/platforms/linux/amd64:$PATH
export KUBE_AWS_ZONE=us-east-1c
export MASTER_SIZE=m3.medium
export NODE_SIZE=t2.medium
export NUM_NODES=3
#export AWS_S3_BUCKET=kubernetes-artifacts
export KUBE_AWS_INSTANCE_PREFIX=kubernetes-001
export VPC_NAME="${KUBE_AWS_INSTANCE_PREFIX}-vpc"
export AWS_SSH_KEY=$HOME/.ssh/kubernetes-001
export MASTER_ROOT_DISK_SIZE=32
export NODE_ROOT_DISK_SIZE=128
export KUBE_ENABLE_INSECURE_REGISTRY=true
#
# Need to do some research if the following has been implemented
#
#export KUBE_ENABLE_CLUSTER_AUTOSCALER=true
#export KUBE_AUTOSCALER_MIN_NODES=3
#export KUBE_AUTOSCALER_MAX_NODES=5
#export KUBE_TARGET_NODE_UTILIZATION=0.7
#

#
# AMS CLI option
#
export AWS_DEFAULT_PROFILE=kubernetes-setup

#
# kube-up option
#
export KUBERNETES_PROVIDER=aws

# skip download
export KUBERNETES_SKIP_DOWNLOAD=true


