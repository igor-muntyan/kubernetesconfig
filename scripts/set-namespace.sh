#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

envs=(dev test stg prod)

for env in "${envs[@]}"
do
  if [[ ${env} == "$1" ]]; then
    export NAMESPACE="$1"
    break
  fi
done

if [ -z "$NAMESPACE" ]
  then
    IFS=,
    echo "specify one of ${envs[*]}"
    exit 1
fi

source set-kubernetes-vars.sh
kubectl create ns "${NAMESPACE}"