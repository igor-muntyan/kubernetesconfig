#!/usr/bin/env bash

source set-kubernetes-vars.sh

# create cluster
${KUBERNETES_HOME}/kubernetes/cluster/kube-up.sh
