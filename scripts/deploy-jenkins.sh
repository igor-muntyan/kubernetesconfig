#!/usr/bin/env bash

source set-namespace.sh

source set-kubernetes-vars.sh


PASSWORD=`openssl rand -base64 15`; echo "Your password is $PASSWORD"; sed -i.bak s#CHANGE_ME#$PASSWORD# jenkins/options
kubectl create secret generic jenkins --from-file=jenkins/options --namespace="${NAMESPACE}"

kubectl apply -f jenkins/persistence.yaml --namespace="${NAMESPACE}"
kubectl apply -f jenkins/service.yaml --namespace="${NAMESPACE}"
kubectl apply -f jenkins/deployment.yaml --namespace="${NAMESPACE}"

cert_file="${HOME}/.ssh/tls.crt"
key_file="${HOME}/.ssh/tls.key"
if [ -f "$cert_file" ] && [ -f "$key_file" ]
then
  kubectl create secret generic tls --from-file="$cert_file" --from-file="$key_file" --namespace="${NAMESPACE}"
else
  echo "$cert_file or $key_file not found."
fi

kubectl apply -f ingress/${NAMESPACE}/ingress-jenkins.yaml --namespace="${NAMESPACE}"
