### Building k8 cluster on AWS

- Install AWS cli
- Configure AWS profile ()
- Add kubernetes script configuration environment variables
- From the `scripts` directory run `./init-kubernetes.sh`
- modify xxx-minion security group in VPC section to enable access to port 80
- Edit Route53
    - Add Hosted Zone 
    - Create A record for the kubernetes master (it must have an EIP address assigned by the kubernetes setup script)
    
### Deploy Ingress Controller
  
- From the `scripts` directory run `./ingress/ingress-controller.sh`

  
### Deploy Jenkins

- Place TLS certificate and key into 
  (or generate you own by):
```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ~/.ssh/tls.key -out ~/.ssh/tls.crt -subj "/CN=*.k8.openlane.net/O=*.k8.openlane.net"
```

- From the `scripts` directory run `./deploy-jenkins.sh`


